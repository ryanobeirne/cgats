//! CGATS data comparison utilities

use crate::*;

impl PartialEq<Self> for DataPoint {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Alpha(s) => other == s,
            Float(f) => other == f,
            Int(i) => other == i,
        }
    }
}

impl Eq for DataPoint {}

impl PartialEq<f32> for DataPoint {
    fn eq(&self, other: &f32) -> bool {
        match self {
            Alpha(s) => if let Ok(f) = s.parse::<f32>() {
                &f == other
            } else {
                false
            }
            Float(f) => f == other,
            Int(i) => &(*i as f32) == other,
        }
    }
}

impl PartialEq<DataPoint> for f32 {
    fn eq(&self, other: &DataPoint) -> bool {
        other == self
    }
}

impl PartialEq<i32> for DataPoint {
    fn eq(&self, other: &i32) -> bool {
        match self {
            Alpha(s) => if let Ok(f) = s.parse::<i32>() {
                &f == other
            } else {
                false
            }
            Float(f) => f == &(*other as f32),
            Int(i) => i == other,
        }
    }
}

impl PartialEq<DataPoint> for i32 {
    fn eq(&self, other: &DataPoint) -> bool {
        other == self
    }
}

impl PartialEq<str> for DataPoint {
    fn eq(&self, other: &str) -> bool {
        if let Ok(other) = other.parse::<f32>() {
            self == &other
        } else {
            match self {
                Alpha(s) => {
                    s == other
                }
                Float(f) => other.parse::<f32>().map(|x| &x == f) == Ok(true),
                Int(i) => other.parse::<i32>().map(|x| &x == i) == Ok(true),
            }
        }
    }
}

impl PartialEq<DataPoint> for str {
    fn eq(&self, other: &DataPoint) -> bool {
        other == self
    }
}

impl PartialEq<DataPoint> for &str {
    fn eq(&self, other: &DataPoint) -> bool {
        other == self
    }
}

impl PartialEq<&str> for DataPoint {
    fn eq(&self, other: &&str) -> bool {
        self == *other
    }
}

impl PartialEq<String> for DataPoint {
    fn eq(&self, other: &String) -> bool {
        self == other.as_str()
    }
}

#[test]
fn data_eq() {
    assert_eq!(Float(42.0), 42.0);
    assert_eq!(42.0, Float(42.0));
    assert_eq!(Alpha("42.000".into()), 42.0);
    assert_eq!(42.0, Alpha("42.00".into()));
    assert_eq!(Alpha("42.000".into()), Float(42.00));
    assert_eq!(Float(42.0), Alpha("42".into()));
    assert_eq!("42", Alpha("42.0".into()));
    assert_eq!("derp", Alpha("derp".into()));
    assert_eq!(Alpha("derp".into()), "derp");
    assert_eq!(Int(42), Float(42.0));
    assert_eq!(Float(42.0), Int(42));

    assert_ne!(Float(42.0), 69.0);
    assert_ne!(Float(42.0), Float(69.0));
    assert_ne!(Float(42.0), Alpha("69.0".into()));
    assert_ne!(Alpha("derp".into()), "flerp");
    assert_ne!("flerp", Alpha("derp".into()));
    assert_ne!("42", Alpha("derp".into()));
    assert_ne!(Alpha("derp".into()), "42");
    assert_ne!(Alpha("derp".into()), 42.0);
    assert_ne!(Alpha("42".into()), "derp");
    assert_ne!(Float(42.0), "derp");

    assert_eq!(Int(42), 42);
    assert_eq!(42, Int(42));
    assert_eq!(Alpha("42".into()), 42);
    assert_eq!(42, Alpha("42".into()));
    assert_eq!(Alpha("42".into()), Int(42));
    assert_eq!(Int(42), Alpha("42".into()));
    assert_eq!("42", Alpha("42".into()));
    assert_eq!("derp", Alpha("derp".into()));
    assert_eq!(Alpha("derp".into()), "derp");

    assert_ne!(Int(42), 69);
    assert_ne!(Int(42), Int(69));
    assert_ne!(Int(42), Alpha("69".into()));
    assert_ne!(Alpha("derp".into()), "flerp");
    assert_ne!("flerp", Alpha("derp".into()));
    assert_ne!("42", Alpha("derp".into()));
    assert_ne!(Alpha("derp".into()), "42");
    assert_ne!(Alpha("derp".into()), 42);
    assert_ne!(Alpha("42".into()), "derp");
    assert_ne!(Int(42), "derp");
}

