//! Vendor specific variables for CGATS files.

use std::{fmt, str::FromStr};
use crate::*;

const KEYWORDS: &[&str] = &[
    "argyll",
    "cti",
    "cgats",
    "colorburst",
    "curve",
    "xrite",
    "x-rite",
    "i1",
    "profiler",
];

/// A vendor is the party that makes the CGATS file with their own special sauce.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Vendor {
    /// ArgyllCMS
    Argyll,
    /// The default CGATS vendor
    Cgats,
    /// ColorBurst RIP Linearization
    ColorBurst,
    /// Curve(v2,3,4) G7 Calibration
    Curve,
    /// XRite i1 Profiler
    Xrite,
}

impl Default for Vendor {
    fn default() -> Self {
        Vendor::Cgats
    }
}

impl Vendor {
    /// Create a new custom Vendor
    pub fn new(s: &str) -> Self {
        Vendor::from(s)
    }
}

impl CgatsFmt for Vendor {
    fn cgats_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Vendor::ColorBurst => writeln!(f, "ColorBurst"),
            Vendor::Curve => writeln!(f, "\"File created by Curve4\""),
            _ => writeln!(f, "CGATS.17"),
        }
    }
}

impl FromStr for Vendor {
    type Err = BoxErr;
    fn from_str(s: &str) -> Result<Self> {
        Ok(Self::from(s))
    }
}

impl From<&str> for Vendor {
    fn from(s: &str) -> Self {
        let s = s.trim();
        if s.is_empty() {
            return Vendor::default();
        }

        for keyword in KEYWORDS.iter() {
            if s.to_lowercase().contains(keyword) {
                let vendor = match *keyword {
                    "argyll" | "cti" => Vendor::Argyll,
                    "cgats" => Vendor::Cgats,
                    "colorburst" => Vendor::ColorBurst,
                    "curve" => Vendor::Curve,
                    "xrite" | "x-rite" | "i1" | "profiler" => Vendor::Xrite,
                    _ => unreachable!("Vendor keyword not in list! [vendor::KEYWORDS]"),
                };

                return vendor;
            }
        }

        Vendor::default()
    }
}

impl fmt::Display for Vendor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self)
    }
}

#[test]
fn from_str() {
    assert_eq!(Vendor::from("ColorBurst"), Vendor::ColorBurst);
    assert_eq!(Vendor::from("CGATS.17"), Vendor::Cgats);
    assert_eq!(
        Vendor::from("File Created by Curve3"),
        Vendor::Curve
    );
    assert_eq!(Vendor::from("CTI1"), Vendor::Argyll);
    assert_eq!(Vendor::from("derp"), Vendor::Cgats,);
    assert_eq!(Vendor::from(""), Vendor::Cgats);
}
